package com.andrewswan.bgg4j;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Test-related utility methods.
 */
public final class TestUtils {

    /**
     * Asserts that the given board game is Die Macher.
     *
     * @param game the game to check
     */
    public static void assertDieMacher(final BoardGame game) {
        assertNotNull(game);
        assertEquals(1, game.getBggId());
        assertEquals("Die Macher", game.getPrimaryName());
        assertEquals(1986, game.getYearPublished());
        assertEquals("https://cf.geekdo-images.com/images/pic159509.jpg", game.getImageUrl());
        assertEquals("https://cf.geekdo-images.com/images/pic159509_t.jpg", game.getThumbnailUrl());
        assertEquals(5, game.getMaxPlayers());
        assertEquals(3, game.getMinPlayers());
        assertEquals("Karl-Heinz Schmiel", game.getDesigner());
    }

    /**
     * Asserts that the given board game is 7 Wonders.
     *
     * @param game the game to check
     */
    public static void assertSevenWonders(final BoardGame game) {
        assertNotNull(game);
        assertEquals(68448, game.getBggId());
        assertEquals("7 Wonders", game.getPrimaryName());

        List<BoardGameExpansion> expansions = game.getExpansions();
        assertEquals(15, expansions.size());
        BoardGameExpansion armada = expansions.get(0);
        assertEquals( 247315, armada.getBggId());
        assertEquals( "7 Wonders: Armada", armada.getName());
    }

    private TestUtils() {}
}
