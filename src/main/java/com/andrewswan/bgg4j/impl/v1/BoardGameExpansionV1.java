package com.andrewswan.bgg4j.impl.v1;

import com.andrewswan.bgg4j.BoardGameExpansion;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;

public class BoardGameExpansionV1 implements BoardGameExpansion {

    @XmlAttribute(name = "objectid", required = true)
    private int bggId;

    @XmlValue
    private String name;

    public int getBggId() {
        return bggId;
    }

    public String getName() {
        return name;
    }
}
