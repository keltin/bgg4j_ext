package com.andrewswan.bgg4j.impl.v1;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;

public class BoardGameNameV1 {
    @XmlAttribute(name = "primary")
    private boolean primary;

    @XmlValue
    private String name;

    public boolean isPrimary() {
        return primary;
    }

    public String getName() {
        return name;
    }

}
