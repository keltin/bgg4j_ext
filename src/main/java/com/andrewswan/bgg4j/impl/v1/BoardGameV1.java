package com.andrewswan.bgg4j.impl.v1;

import com.andrewswan.bgg4j.BoardGame;
import com.andrewswan.bgg4j.BoardGameExpansion;
import com.andrewswan.bgg4j.BoardGameSummary;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import java.util.ArrayList;
import java.util.List;

/**
 * A board game on BGG, obtained via version 1 of the BGG XML API.
 *
 * @since 1.0
 */
public class BoardGameV1 implements BoardGame {

    @XmlAttribute(name = "objectid", required = true)
    private int bggId;

    @XmlElement(name = "maxplayers")
    private int maxPlayers;

    @XmlElement(name = "minplayers")
    private int minPlayers;

    @XmlElement(name = "yearpublished")
    private int yearPublished;

    @XmlElement(name = "name")
    private List<BoardGameNameV1> names;

    @XmlElement(name = "boardgamedesigner")
    private String designer;

    @XmlElement(name = "image")
    private String imageUrl;

    @XmlElement(name = "thumbnail")
    private String thumbnailUrl;

    @XmlElement(name = "boardgameexpansion")
    private List<BoardGameExpansionV1> expansions;

    public int getBggId() {
        return bggId;
    }

    public String getDesigner() {
        return designer;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public int getMaxPlayers() {
        return maxPlayers;
    }

    public int getMinPlayers() {
        return minPlayers;
    }

    public String getPrimaryName() {

        if (names == null || names.isEmpty()) {
            return null;
        }

        for (BoardGameNameV1 n : names) {
            if (n.isPrimary()) {
                return n.getName();
            }
        }

        return names.get(0).getName();
    }

    public int getYearPublished() {
        return yearPublished;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public List<BoardGameExpansion> getExpansions() { return new ArrayList<BoardGameExpansion>(expansions); }

    /**
     * Returns summary information about this game.
     *
     * @return a non-null summary
     */
    public BoardGameSummary getSummary() {
        return new BoardGameSummaryV1(bggId, yearPublished, getPrimaryName());
    }
}
