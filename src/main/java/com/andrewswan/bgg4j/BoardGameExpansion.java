package com.andrewswan.bgg4j;


public interface BoardGameExpansion {
    int getBggId();

    String getName();
}
